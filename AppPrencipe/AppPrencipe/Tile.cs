﻿using System;
using System.Collections.Generic;

namespace AppPrencipe
{
    public class Tile
    {

        private const int TILE2 = 2;
        private const int TILE4 = 4;
        private const double LIMIT = 0.3;
        private readonly int value;
        private KeyValuePair<int, int> position;
        private bool merged;

        /// <summary>
        /// Constructor.
        /// </summary>
        public Tile(int value)
        {
            this.position = new KeyValuePair<int,int>(-1,-1);
            this.value = value;
            this.merged = false;
        }

        /// <summary>
        /// This method create a new Tile, with the lowest value of the powers of 2.
        /// The value of the new tile is chosen randomly between 2 or 4.
        /// </summary>
        /// <returns> Tile
        ///  </returns>
        public static Tile NewRandomTile()
        {
            Random random = new Random();
            return new Tile(random.NextDouble() < LIMIT ? TILE2 : TILE4);
        }

        public virtual int Value
        {
            get
            {
                return value;
            }
        }

        public virtual KeyValuePair<int, int> Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
            }
        }

        public virtual bool Merged
        {
            set
            {
                merged = value;
            }
            get
            {
                return merged;
            }
        }

    }
}