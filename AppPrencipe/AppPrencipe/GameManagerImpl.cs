﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppPrencipe
{
    public class GameManagerImpl : IGameManager
    {
        private const int MAX_VALUE_TO_WIN = 2048;

        /// <summary>
        /// This field is the grid of the game.
        /// </summary>
        protected internal IDictionary<KeyValuePair<int, int>, Tile> grid;

        /// <summary>
        /// This field count the score of the game.
        /// </summary>
        protected internal int myScore;

        /// <summary>
        /// This contains the free positions of the game.
        /// </summary>
        protected internal ISet<KeyValuePair<int, int>> positions = new HashSet<KeyValuePair<int, int>>();

        private readonly int gridSize;
        private List<int> traversalX;
        private List<int> traversalY;

        public int Score => this.myScore;

        public IDictionary<KeyValuePair<int, int>, Tile> Grid => this.grid;

        public GameStatus GameStatus { get; private set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        public GameManagerImpl(int gridSize)
        {
            this.GameStatus = GameStatus.PLAYING;
            this.gridSize = gridSize;
        }

        public virtual void InitGrid()
        {
            this.grid = new Dictionary<KeyValuePair<int, int>, Tile>();
            this.traversalX = new List<int>();
            traversalY = new List<int>();
            for (int i = 0; i < gridSize; i++)
            {
                traversalX.Add(i);
                traversalY.Add(i);
            }
            grid.Clear();
            positions.Clear();
            foreach (int x in traversalX)
            {
                foreach (int y in traversalY)
                {
                    positions.Add(new KeyValuePair<int,int>(x, y));
                    grid[new KeyValuePair<int,int>(x, y)] = null;
                }
            }
        }  

        public virtual void StartGame()
        {

            KeyValuePair<int, int> position0 = FindRandomFreePosition();
            Tile tile0;
            Tile tile1;
            KeyValuePair<int, int> position1 = FindRandomFreePosition();
            while (position0.Equals(position1))
            {
                position1 = FindRandomFreePosition();
            }
            tile0 = Tile.NewRandomTile();
            tile1 = Tile.NewRandomTile();

            tile0.Position = position0;
            tile1.Position = position1;
            List<Tile> tileList = new List<Tile>()
            {
                tile0,
                tile1
            };

            foreach (Tile t in tileList)
            {
                grid[t.Position] = t;
            }
        }

        public virtual void AddNewTile(KeyValuePair<int, int> randomPosition)
        {
            var tile = Tile.NewRandomTile();
            tile.Position = randomPosition;
            grid[randomPosition] = tile;
        }

        /// <summary>
        /// Sort traverse if Direction is RIGHT or DOWN.
        /// </summary>
        private void SortGrid(Direction direction)
        {
            if (direction.Equals(Direction.RIGHT))
            {
                traversalX.Reverse();
            }
            else if (direction.Equals(Direction.DOWN))
            {
                traversalY.Reverse();
            }
            else if (direction.Equals(Direction.LEFT))
            {
                traversalX.Sort();
            }
            else if (direction.Equals(Direction.UP))
            {
                traversalY.Sort();
            }
        }

        public virtual void MoveTiles(Direction direction)
        {
            myScore = 0;
            SortGrid(direction);
            foreach (int x in traversalX)
            {
                foreach (int y in traversalY)
                {
                    KeyValuePair<int, int> thisPos = new KeyValuePair<int, int>(x, y);
                    Tile tile = TileInNextPosition(thisPos);
                    KeyValuePair<int, int> farthest = FindFarthestPosition(thisPos, direction);
                    if (tile != null && !farthest.Equals(thisPos))
                    {
                        grid[farthest] = tile;
                        grid[thisPos] = null;
                        tile.Position = farthest;
                    }
                };
            };
            List<Tile> tileList = new List<Tile>(grid.Values.Where(o=>o!=null));
            foreach (Tile t in tileList)
            {
                t.Merged = false;
            }
        }

        private KeyValuePair<int, int> FindFarthestPosition(KeyValuePair<int, int> thisPos, Direction direction)
        {
            KeyValuePair<int, int> farthest;
            KeyValuePair<int, int> position = thisPos;
            do
            {
                farthest = position;
                position = Offset(farthest,direction);
            } while (IsValidPosition(position) && TileInNextPosition(position) == null) ;

            return farthest;
        }

        private KeyValuePair<int, int> Offset(KeyValuePair<int, int> thisPos, Direction direction)
        {
            return new KeyValuePair<int, int>(thisPos.Key + direction.X,thisPos.Value + direction.Y);
        }


        private bool IsValidPosition(KeyValuePair<int, int> position)
        {
            return position.Key >= 0 && position.Key < gridSize && position.Value >= 0 && position.Value < gridSize-1;
        }

        public virtual bool LostControl()
        {
            if (FindRandomFreePosition().Equals(new KeyValuePair<int, int>(-1, -1)))
            {
                if (!IsDirectionMergePossible(Direction.LEFT) && !IsDirectionMergePossible(Direction.RIGHT) && !IsDirectionMergePossible(Direction.UP) && !IsDirectionMergePossible(Direction.DOWN))
                {
                    GameStatus = GameStatus.LOST;
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// This method return true if there is merging possible.
        /// </summary>
        private bool IsDirectionMergePossible(Direction direction)
        {
            int mergePossible = 0;
            SortGrid(direction);
            foreach (int x in traversalX)
            {
                foreach (int y in traversalY)
                {
                    KeyValuePair<int, int> thisPos = new KeyValuePair<int, int>(x, y);
                    Tile tile = TileInNextPosition(thisPos);
                    KeyValuePair<int, int> nextPosition = Offset(thisPos,direction);
                    if (TileInNextPosition(nextPosition)!=null && tile!=null && IsMergeable(thisPos, nextPosition))
                    {
                        mergePossible++;
                    }
                }
            }
            return mergePossible != 0;
        }

        /// <summary>
        /// This method return new tile merged with another. </summary>
        /// <returns> tile </returns>
        private Tile Merge(int value1, int value2, KeyValuePair<int, int> nextPosition)
        {
            int res = value1 + value2;
            Tile tile = new Tile(res)
            {
                Merged = true
            };
            grid[nextPosition] = tile;
            tile.Position = nextPosition;
            return tile;
        }

        public virtual bool IsMergeable(KeyValuePair<int, int> thisPos, KeyValuePair<int, int> nextPosition)
        {
            Tile first = TileInNextPosition(thisPos);
            Tile second = TileInNextPosition(nextPosition);
            return first.Value == second.Value;
        }

        private Tile TileInNextPosition(KeyValuePair<int, int> nextPosition)
        {
            return grid[nextPosition];
        }

        public KeyValuePair<int, int> FindRandomFreePosition()
        {
            Random rnd;
            rnd = new Random();
            IList<KeyValuePair<int, int>> freePositions = new List<KeyValuePair<int, int>>(positions.Where(l => grid[l] == null).ToList());
            
            if (freePositions.Count == 0)
            {
                return new KeyValuePair<int, int>(-1,-1);
            }
            return freePositions[rnd.Next(freePositions.Count)];
        }

        public bool WinControl()
        {
            return GameStatus == GameStatus.WIN;
        }
    }
}
