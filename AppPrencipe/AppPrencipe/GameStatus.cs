﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppPrencipe
{   
    /// <summary>
    /// Enumeration for the game status.
    /// 
    /// </summary>
    public enum GameStatus
    {
        EXIT,
        PLAYING,
        WIN,
        LOST
    }
}
