﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace AppPrencipe
{
    public interface IGameManager
    {       
        /// <summary>
        /// This method initialize a new grid with its positions.
        /// </summary>
        void InitGrid();

        /// <summary>
        /// This method start a new game with two new tile in two random positions.
        /// </summary>
        void StartGame();

        /// <summary>
        /// This method deals with add a new tile in random position.
        /// </summary>
        void AddNewTile(KeyValuePair<int, int> randomPosition);

        /// <summary>
        /// This method moves the tiles from a starting position towards its farthest
        /// square in the direction with the necessary checks.
        /// </summary>
        void MoveTiles(Direction direction);

        /// <summary>
        /// This method the score update.
        /// </summary>
        /// <returns> score </returns>
        int Score { get; }

        /// <summary>
        /// This method grid with updated positions.
        /// </summary>
        /// <returns> grid </returns>
        IDictionary<KeyValuePair<int, int>, Tile> Grid { get; }

        /// <summary>
        /// This method return the game status.
        /// </summary>
        /// <returns> gameStatus </returns>
        GameStatus GameStatus { get; }

        /// <summary>
        /// This method return a new random free position.
        /// </summary>
        /// <returns> position </returns>
        KeyValuePair<int, int> FindRandomFreePosition();


        /// <summary>
        /// This method if a tile is merge able.
        /// </summary>
        /// <returns> boolean </returns>
        bool IsMergeable(KeyValuePair<int, int> thisPos, KeyValuePair<int, int> nextPosition);

        /// <summary>
        /// This method return true if you have won.
        /// </summary>
        /// <returns> boolean </returns>
        bool WinControl();

        /// <summary>
        /// This method return true if you have lost.
        /// </summary>
        /// <returns> boolean </returns>
        bool LostControl();

    }
}
