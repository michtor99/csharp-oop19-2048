﻿using System.Collections.Generic;

namespace AppPrencipe
{
    public class Direction
    {       

        /// <summary>
        /// This direction moves up a tile.
        /// </summary>
        public static readonly Direction UP = new Direction("UP", DirectionEnum.UP, 0, -1);

        /// <summary>
        /// This direction moves right a tile.
        /// </summary>
        public static readonly Direction RIGHT = new Direction("RIGHT", DirectionEnum.RIGHT, 1, 0);

        /// <summary>
        /// This direction moves down a tile.
        /// </summary>
        public static readonly Direction DOWN = new Direction("DOWN", DirectionEnum.DOWN, 0, 1);

        /// <summary>
        /// This direction moves left a tile.
        /// </summary>
        public static readonly Direction LEFT = new Direction("LEFT", DirectionEnum.LEFT, -1, 0);

        private static readonly List<Direction> valueList = new List<Direction>();

        static Direction()
        {
            valueList.Add(UP);
            valueList.Add(RIGHT);
            valueList.Add(DOWN);
            valueList.Add(LEFT);
        }

        public enum DirectionEnum
        {
            UP,
            RIGHT,
            DOWN,
            LEFT
        }

        public readonly DirectionEnum directionEnumValue;
        private readonly string nameValue;

        internal Direction(string name, DirectionEnum directionEnum, int x, int y)
        {
            this.X = x;
            this.Y = y;

            nameValue = name;
            directionEnumValue = directionEnum;
        }

        public int X { get; }

        public int Y { get; }

    }
}