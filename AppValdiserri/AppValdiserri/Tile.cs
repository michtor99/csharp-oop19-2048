﻿using System;
using System.Collections.Generic;

namespace AppValdiserri
{

        interface ITile
    {
            /// <summary>
            /// This method return the value of the tile.
            /// </summary>
            /// <returns></returns>
            int Value { get; }

            /// <summary>
            /// This method set end return the position of the Tile.
            /// </summary>
            /// <returns></returns>
            KeyValuePair<int, int> Position { get; }

            /// <summary>
            /// This method sets and return the status of Tile.
            /// </summary>
            /// <returns></returns>
            bool Merged { get; }

        }

        public class Tile : ITile
        {

            private static int TILE2 = 2;

            private static int TILE4 = 4;

            private static double LIMIT = 0.3;

            private int value;

            private KeyValuePair<int, int> position;

            private bool merged;

            /// <summary>
            /// Constructor.
            /// </summary>
            /// <param name="value"></param>
            public Tile(int value)
            {
                this.position = new KeyValuePair<int, int>(-1, -1);
                this.value = value;
                this.merged = false;
            }

            /// <summary>
            /// This method return a Tile with value 0, this tile is empty.
            /// </summary>
            /// <returns></returns>
            public static Tile Empty()
            {
                return new Tile(0);
            }

            /// <summary>
            /// This method create a new Tile, with the lowest value of the powers of 2.
            /// The value of the new tile is chosen randomly between 2 or 4.
            /// </summary>
            /// <returns></returns>
            public static Tile NewRandomTile()
            {
            Random random = new Random();
            return new Tile(random.NextDouble() < LIMIT ? TILE2 : TILE4);
            }

            public int Value
            {
                get { return this.value; }
            }

            public KeyValuePair<int,int> Position
            {
                get { return this.position; }
                set { this.position = value; }
            }

            public bool Merged
            {
                get { return this.merged; }
                set { this.merged = value; }
            }

        }


    }
