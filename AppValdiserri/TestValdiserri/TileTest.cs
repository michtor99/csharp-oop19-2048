﻿using System;
using AppValdiserri;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestValdiserri
{
    [TestClass]
            public class TileTest
            {
                [TestMethod]
                public void TestInizitializeNewTileValue()
                {
                    Tile tile = new Tile(512);
                    Assert.AreEqual(tile.Value,512);
                }

                [TestMethod]
                public void TestInizitializeNewTileRandom()
                {
                    Tile tile = Tile.NewRandomTile();
                    Assert.IsTrue(tile.Value.Equals(2) || tile.Value.Equals(4));
                }
            }

}