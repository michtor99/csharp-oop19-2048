using System;
using System.Collections.Generic;
using System.Linq;
using AppMarconi;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestAppMarconi
{

    [TestClass]
    public class AlternativeManagerTest
    {
        [TestMethod]
        public void TestInitialize()
        {
            AlternativeManagerImpl am = new AlternativeManagerImpl(4);
            am.InitTilesGrid();
            Assert.IsNotNull(am);
        }

        [TestMethod]
        public void TestAddAlternativeTile()
        {
            AlternativeManagerImpl am = new AlternativeManagerImpl(4);
            am.InitTilesGrid();
            KeyValuePair<int, int> pos = new KeyValuePair<int, int>(0, 0);
            am.AddAlternativeTile(2, pos);
            IDictionary<KeyValuePair<int, int>, AlternativeTile> grid;
            grid = am.GetAlternativeGrid();
            int value= grid[pos].GetValue();
            Assert.IsTrue(value.Equals(2));
        }

        [TestMethod]
        public void TestControlAlternativeTilePos()
        {
            AlternativeManagerImpl am = new AlternativeManagerImpl(4);
            am.InitTilesGrid();
            KeyValuePair<int, int> pos = new KeyValuePair<int, int>(0, 0);
            am.AddAlternativeTile(2, pos);
            Tile tile = new Tile(8, pos);
            am.AddTile(tile, pos);
            am.ControlAlternativePos();
            IDictionary<KeyValuePair<int, int>, Tile> tileGrid = am.GetTileGrid();
            int value = tileGrid[pos].GetValue();
            Assert.AreEqual(value, 16);
        }
    }
    
}
