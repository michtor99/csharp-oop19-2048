﻿using System.Collections.Generic;
namespace AppMarconi

{
    public class Tile
    {
        private int Value;
        private KeyValuePair<int, int> Position;
        public Tile(int value, KeyValuePair<int,int> pos)
        {
            Value = value;
            Position = pos;
        }

        public int GetValue()
        {
            return Value;
        }
    }
}