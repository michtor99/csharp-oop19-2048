﻿using com.sun.org.apache.bcel.@internal.generic;
using java.util.stream;
using System.Collections.Generic;

namespace AppMarconi
{
	/// <summary>
	/// This class manage the alternative game mode, extending the Game Manager
	/// class.
	/// </summary>
	public class AlternativeManagerImpl
	{
		private IDictionary<KeyValuePair<int, int>, AlternativeTile> alternativeTilesGrid;
		private IDictionary<KeyValuePair<int, int>, Tile> grid;
		private IList<KeyValuePair<int,int>> positions;
		private readonly int size;

		/// <summary>
		/// Constructor.
		/// </summary>
		public AlternativeManagerImpl(int gridSize)
		{
			size = gridSize;
		}

		/// <summary>
		/// This method initialize the alternative tile grid.
		/// </summary>
		public void InitTilesGrid()
		{
			alternativeTilesGrid = new Dictionary<KeyValuePair<int, int>, AlternativeTile>();
			grid = new Dictionary<KeyValuePair<int, int>, Tile>();
			positions = new List<KeyValuePair<int, int>>();
			for (int x=0; x<=size; x++) {
				for (int y = 0; y <= size; y++)
				{
					KeyValuePair<int, int> pos = new KeyValuePair<int, int>(x, y);
					grid[pos] = null;
					positions.Add(pos);
					alternativeTilesGrid[pos] = null;
				}
			}
		}

		/// <summary>
		/// This method add an alternative tile in the grid.
		/// </summary>
		public void AddAlternativeTile(int value, KeyValuePair<int, int> randomPosition)
		{
			AlternativeTile alternativeTile;
			alternativeTile = new AlternativeTile(value, randomPosition);
			alternativeTilesGrid[randomPosition] = alternativeTile;
		}

		/// <summary>
		/// This method control if a tile is in the same position of an alternative
		/// tile. If a tile is on a alternative tile x0 it remove the tile else if is
		/// on a x2 tile it create a new tile with the old value multiplied by two.
		/// </summary>
		public void ControlAlternativePos()
		{
			for (int x = 0 ; x <= size; x++)
			{
				for (int y = 0; y <= size; y++)
                {
					KeyValuePair<int, int> pos = new KeyValuePair<int, int>(x, y);
                    if (grid[pos] != null && alternativeTilesGrid[pos] != null)
                    {
                        int value = (grid[pos].GetValue()) * (alternativeTilesGrid[pos].GetValue());
                        if (value.Equals(0))
                        {
                            grid[pos] = null;
                        }
                        else
                        {
                            Tile tile = new Tile(value, pos);
                            grid[pos] = tile;
                        }
                        alternativeTilesGrid[pos] = null;
                    }
                }
            }
		}

		/// <summary>
		/// This method return the alternative tile grid.
		/// </summary>
		/// <returns> AlternativeGrid </returns>
		public IDictionary<KeyValuePair<int, int>, AlternativeTile> GetAlternativeGrid()
		{
			return alternativeTilesGrid;
		}

		/// <summary>
		/// This method return the grid.
		/// </summary>
		/// <returns> AlternativeGrid </returns>
		public IDictionary<KeyValuePair<int, int>, Tile> GetTileGrid()
        {
			return grid;
		}

		/// <summary>
		/// This add a Tile in the grid.
		/// </summary>
		/// <returns> AlternativeGrid </returns>
		public void AddTile(Tile tile, KeyValuePair<int,int> pos)
        {
			grid[pos] = tile;
        }

	}
}