﻿using System.Collections.Generic;
namespace AppMarconi

{
    public class AlternativeTile
    {
        private int Value;
        private KeyValuePair<int, int> Position;

        public AlternativeTile(int value, KeyValuePair<int, int> randomPosition)
        {
            Value = value;
            Position = randomPosition;
        }

        public int GetValue()
        {
            return Value;
        }
    }
   
}